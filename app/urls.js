
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , path = require('path')
  , mongoose = require('mongoose')
  , passport = require('passport');

/*!
 * Controllers.
 */

var Users = require(CONFIG.__controllers + '/users')
  , Tokens = require(CONFIG.__controllers + '/tokens')
  , Applications = require(CONFIG.__controllers + '/applications');

/*!
 * Helpers.
 */

var acceptAllOrigins = function (req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Headers", "X-Requested-With");
  next();
};

var ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  req.session.page = req.url;
  res.redirect('/');
};

var authenticate = passport.authenticate(['token'], { session: false })

/*!
 * Routes.
 */

module.exports = function (app) {
  app.get('/', function (req, res, next) {
    res.send({ _ok: 1, message: 'Hello world!'})
  });

  app.namespace('/api', function () {
    app.resource('applications', Applications);
    app.resource('tokens', Tokens);
    app.resource('users', Users);
  });
};
