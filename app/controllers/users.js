
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , Forbidden = require(CONFIG.__helpers + '/error').Forbidden
  , NotFound = require(CONFIG.__helpers + '/error').NotFound
  , mongoose = require('mongoose');

/**
 * Models
 */

var User = mongoose.model('User');

exports.id = '_user';

/**
 * Get users.
 */

exports.index = function (req, res, next) {
  req.user.should(next, 'users.view');

  User.find(req.filter.q)
  .limit(req.filter.limit)
  .skip(req.filter.skip)
  .sort(req.filter.sort)
  .exec(function (err, users) {
    res.json({ "_ok": 1, users: users, page: req.filter.page, limit: req.filter.limit, skip: req.filter.skip });
  });
};

/**
 * Get users.
 */

exports.show = function (req, res, next) {
  req.user.should(next, 'users.view');
  res.json({ "_ok": 1, user: req._user });
}

/**
 * Auto-load user for id
 */

exports.load = function (req, id, next) {
  if (!req.user) return next(new Forbidden('Нет прав'));
  console.log(req.user);
  User.findById(id, function (err, user) {
    if (!user) return next(new NotFound());
    next(err, user);
  });
};
