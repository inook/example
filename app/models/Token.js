
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , _ = require('underscore')
  , mongoose = require('mongoose')
  , ObjectId = mongoose.Schema.Types.ObjectId
  , debug = require('debug')('Model:Token')
  , roles = require('roles')
  , uuid = require('node-uuid').v4;

/*!
 * Statuses.
 */

var statuses = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DELETED: 'deleted'
}

var STATUSES = _.values(statuses);

/*!
 * Token schema.
 */

var Token = mongoose.Schema({
  token:            { type: String, unique: true, trim: true },
  _user:            { type: ObjectId, ref: 'User' },
  _client:          { type: String },
  created_at:       { type: Date, 'default': Date.now },
  updated_at:       { type: Date, 'default': Date.now },
  status:           { type: String, 'enum': STATUSES, 'default': STATUSES[0] }
});

/*!
 * Expose statuses.
 */

Token.statics.STATUSES = STATUSES;

_.extend(Token.statics, statuses);

/*!
 * Init model.
 */

try {
  mongoose.model('Token', Token);
} catch (error) {}

/*!
 * Expose token model.
 */

module.exports = mongoose.model('Token');
