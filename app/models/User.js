
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , _ = require('underscore')
  , mongoose = require('mongoose')
  , privatePaths = require('mongoose-private-paths')
  , Forbidden = require(CONFIG.__helpers + '/error').Forbidden
  , debug = require('debug')('Model:User')
  , roles = require('roles')
  , uuid = require('node-uuid').v4;

/*!
 * Statuses.
 */

var statuses = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  RESTORE: 'restore',
  BLOCKED: 'blocked'
}

var STATUSES = _.values(statuses);

/*!
 * User schema.
 */

var User = mongoose.Schema({
  email:            { type: String, 'private': true, unique: true, lowercase: true, trim: true },
  name:             { type: String },
  facebookId:       { type: String },
  role:             { type: String, 'private': true, 'default': 'client', required: true },
  created_at:       { type: Date, 'private': true, 'default': Date.now },
  token:            { type: String, 'default': function () { return uuid(); } },
  status:           { type: String, 'enum': STATUSES, 'default': STATUSES[0] }
});

/*!
 * Private fileds plugin
 */

User.plugin(privatePaths);

/*!
 * Static methods.
 */

User.statics.findByFacebookId = function (facebookId, cb) {
  this.findOne({ facebookId: facebookId, status: { "$in": [ACTIVE, INACTIVE, RESTORE] } }, function (err, user) {
    if (err || !user) { return cb(err || true); }
    cb(null, user);
  });
};

User.statics.findByToken = function (token, cb) {
  this.findOne({ token: token, status: { "$in": STATUSES.slice(0, -1) } }, function (err, user) {
    if (err || !user) { return cb(err || true); }
    cb(null, user);
  });
};

/*!
 * User methods.
 */

User.methods.is = function (role) {
  return this.role === role;
};

User.methods.can = function () {
  if (this.role === null) {
    return false;
  }
  if (this.role === 'admin') {
    return true;
  }
  var role = roles.getProfile(this.role);
  return role.hasRoles.apply(role, arguments);
};

User.methods.canAny = function () {
  if (this.role === null) {
    return false;
  }
  if (this.role === 'admin') {
    return true;
  }
  var role = roles.getProfile(this.role);
  return role.hasAnyRoles.apply(role, arguments);
};

User.methods.should = function (next) {
  var args = Array.prototype.slice.call(arguments, 1);
  if (!this.can.apply(this, args)) {
    next(new Forbidden('Нет прав'));
  }
};

User.methods.shouldAny = function (next) {
  var args = Array.prototype.slice.call(arguments, 1);
  if (!this.canAny.apply(this, args)) {
    next({ name: 'Forbidden', message: 'Forbidden' });
  }
};

/*!
 * Expose statuses.
 */

User.statics.STATUSES = STATUSES;

_.extend(User.statics, statuses);

/*!
 * Init model.
 */

try {
  mongoose.model('User', User);
} catch (error) {}

/*!
 * Expose user model.
 */

module.exports = mongoose.model('User');

