
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , _ = require('underscore')
  , mongoose = require('mongoose')
  , ObjectId = mongoose.Schema.Types.ObjectId
  , debug = require('debug')('Model:Token')
  , roles = require('roles')
  , uuid = require('node-uuid').v4;

/*!
 * Statuses.
 */

var statuses = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  DELETED: 'deleted'
}

var STATUSES = _.values(statuses);

/*!
 * Token schema.
 */

var Application = mongoose.Schema({
  name:             { type: String },
  secret:           { type: String, 'default': function () { return uuid(); } },
  redirect:         { type: String },
  _user:            { type: ObjectId, ref: 'User' },
  created_at:       { type: Date, 'default': Date.now },
  status:           { type: String, 'enum': STATUSES, 'default': STATUSES[0] }
});

/*!
 * Expose statuses.
 */

Application.statics.STATUSES = STATUSES;

_.extend(Application.statics, statuses);

/*!
 * Init model.
 */

try {
  mongoose.model('Application', Application);
} catch (error) {}

/*!
 * Expose application model.
 */

module.exports = mongoose.model('Application');
