
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , _ = require('underscore')
  , Err = require('../error');

/**
 * Forbidden Error
 *
 * @param {String|Object} User.STATUSES или Error
 * @param {String|Array} Сообщение или аргументы
 * @param {Array} Аргументы
 * @inherits Err
 * @api private
 */

function Forbidden(err, msg, args) {
  if (_.isArray(msg)) {
    args = msg;
    msg = '';
  }

  Err.call(this, err.message || msg || '403 Forbidden');
  Error.captureStackTrace(this, arguments.callee);

  if (_.isObject(err)) _.extend(this, err);
  else this.error = err;

  this.name = 'Forbidden';
  this._ok = 0;

  //if (args)
    //Notifier.publish('user error', this);
};

/*!
 * Inherits from MongooseError.
 */

Forbidden.prototype.__proto__ = Err.prototype;

/*!
 * Module exports.
 */

module.exports = exports = Forbidden;
