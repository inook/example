
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , _ = require('underscore')
  , Err = require('../error');

/**
 * NotFound Error
 *
 * @param {String|Array} Сообщение или аргументы
 * @param {Array} Аргументы
 * @inherits Err
 * @api private
 */

function NotFound(err, msg) {
  Err.call(this, err.message || msg || '404 Not Found');
  Error.captureStackTrace(this, arguments.callee);

  if (_.isObject(err)) _.extend(this, err);
  else this.error = err;

  this.name = 'NotFound';
  this._ok = 0;
};

/*!
 * Inherits from MongooseError.
 */

NotFound.prototype.__proto__ = Err.prototype;

/*!
 * Module exports.
 */

module.exports = exports = NotFound;
