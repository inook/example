
/**
 * Module dependencies.
 */

var CONFIG = require('config')
  , Internal = require(CONFIG.__helpers + '/error').Internal
  , _ = require('underscore');

/**
 * Filter middleware
 */

exports.filterHandler = function (req, res, next) {

  // Params.
  var status = req.param('status', false)
    , page = req.param('page', 1)
    , limit = req.param('limit', 50)
    , sortBy = req.param('sortby', 'created_at')
    , sortDirection = req.param('sort_direction', 'desc');

  // Filter.
  req.filter = {
    q: {},
    fields: req.param('fields', ''),
    page: page,
    limit: limit,
    skip: req.param('skip', (page - 1) > 0 ? (page - 1) * limit : 0),
    sortBy: sortBy,
    sortDirection: sortDirection,
    sort: req.param('sort', (sortDirection !== 'asc' ? '-': '') + sortBy).replace(',', ' ')
  };

  // Filter status.
  if (status) {
    req.filter.q['status'] = { $in : status.split(',') };
  } else {
    req.filter.q['status'] = 'active';
  }

  delete status, page, limit, sortBy, sortDirection;

  next();
};

exports.errorHandler = function (err, req, res, next) {
  if (err.name === 'NotFound') {
    res.status(404);
    res.json(err);
  } else if (err.name === 'Forbidden') {
    res.status(403);
    res.json(err);
  } else {
    res.status(500);
    console.log(err.captureStackTrace);
    res.json();
  }
};
