
/*!
 * Module dependencies.
 */

/**
 * Err constructor
 *
 * @param {String} msg Error message
 * @inherits Error https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error
 */

function Err (msg) {
  Error.call(this);
  Error.captureStackTrace(this, arguments.callee);
  this.message = msg;
  this.name = 'Error';
};

/*!
 * Inherits from Error.
 */

Err.prototype.__proto__ = Error.prototype;

/*!
 * Module exports.
 */

module.exports = exports = Err;

/*!
 * Expose subclasses
 */

Err.Forbidden = require('./errors/forbidden');
