
/*!
 * Module dependencies.
 */

var fs = require('fs')
  , path = require('path')
  , http = require('http')
  , CONFIG = require('config')
  , express = require('express')
  , namespace = require('express-namespace')
  , resource = require('express-resource')
  , mongoose = require('mongoose')
  , MongoStore = require('connect-mongo')(express);

/*!
 * Paths.
 */

var argv = CONFIG.argv
  , __app = CONFIG.__app
  , __project = CONFIG.__project
  , __configs = CONFIG.__configs
  , __models = CONFIG.__models
  , __helpers = CONFIG.__helpers
  , __events = CONFIG.__events
  , __environments = CONFIG.__environments;

/**
 * Helpers.
 */

var middleware = require(__helpers + '/middleware');

/*!
 * RegExps.
 */

var jsFileRegexp = new RegExp('^[\\w]+\\.js$');

/*!
 * Test environment without Mongoose connenct.
 */

if (process.env.NODE_ENV !== 'test')
  mongoose.connect(CONFIG.mongodb.host, CONFIG.mongodb.database);

/*!
 * Load models.
 */

fs.readdirSync(__models).forEach(function (modelName) {
  if (jsFileRegexp && !jsFileRegexp.test(modelName)) return;
  require(path.join(__models, modelName));
});

/*!
 * Load events.
 */

fs.readdirSync(__events).forEach(function (name) {
  if (jsFileRegexp && !jsFileRegexp.test(name)) return;
  require(path.join(__events, name));
});

/*!
 * Initializers.
 */

var passport = require(path.join(__configs, 'initializers/auth'))
  , roles = require(path.join(__configs, 'initializers/roles'))
  , app = express()
  , server = http.createServer(app)
  , sessionStore = new MongoStore({ db: CONFIG.mongodb.database });

/*!
 * Load rules.
 */

roles.reloadRoles();

/*!
 * Application configure.
 */

app.configure(function () {
  app.set('port', argv.port);
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({
    secret: 'ExampleApplication',
    store: sessionStore
  }));
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(middleware.filterHandler);
  app.use(app.router);
  app.use(middleware.errorHandler);
});

/*!
 * Load environments.
 */

fs.readdirSync(__environments).forEach(function (name) {
  if (jsFileRegexp && !jsFileRegexp.test(name)) return;
  var file = require(path.join(__environments, name));
  file(app);
});


/*!
 * Load controllers.
 */

require(path.join(__app, 'urls'))(app);

/*!
 * Listen port.
 */

if (!module.parent) {
  server.listen(app.get('port'), function () {
    console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
  });
}

/*!
 * Expose HTTP server.
 */

exports = module.exports = server;

/*!
 * Expose application.
 */

exports.app = app;

