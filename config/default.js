
/*!
 * Module dependencies.
 */

var path = require('path');

/*!
 * Expose argv.
 */

var argv = exports.argv = {
  port: process.env.PORT || 8000
};

/*!
 * Expose paths.
 */

var __project       = exports.__project       = path.join(__dirname, '../')
  , __app           = exports.__app           = path.join(__project, 'app')
  , __configs       = exports.__configs       = path.join(__project, 'config')
  , __helpers       = exports.__helpers       = path.join(__app, 'helpers')
  , __events        = exports.__events        = path.join(__app, 'events')
  , __controllers   = exports.__controllers   = path.join(__app, 'controllers')
  , __models        = exports.__models        = path.join(__app, 'models')
  , __initializers  = exports.__initializers  = path.join(__configs, 'initializers')
  , __strategies    = exports.__strategies    = path.join(__initializers, 'strategies')
  , __components    = exports.__components    = path.join(__app, 'components')
  , __tests         = exports.__tests         = path.join(__project, 'tests')
  , __libs          = exports.__libs          = path.join(__project, 'libs')
  , __environments  = exports.__environments  = path.join(__configs, 'environments');

/*!
 * Expose locale.
 */

var __locale = exports.__locale = 'ru';

/*!
 * Facebook config.
 */

exports.facebook = {
  clientID: '1384895831769222',
  clientSecret: 'da3f89e7a04c706cc4f385fff842b1e5',
  callbackURL: 'http://localhost:8000/auth/facebook/callback'
}

/*!
 * MongoDB config.
 */

exports.mongodb = {
  host: '<host>',
  port: '<port>',
  user: '',
  password: '',
  database: '<database>'
}
