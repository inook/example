
/*!
 * Module dependencies.
 */

var path = require('path');

/*!
 * MongoDB config.
 */

exports.mongodb = {
  host: 'localhost',
  port: 27017,
  user: '',
  password: '',
  database: 'example_dev'
};

