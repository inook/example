
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , roles = require("roles")
  , fs = require('fs')
  , rolesConfigPath = CONFIG.__configs + '/auth_roles.json';

/**
 * Reload roles on roles.js
 *
 * @return {void}
 */

var reloadRoles = function(cb) {
  var actions, record = fs.readFileSync(rolesConfigPath, 'utf8');

  if (record === null || record === undefined)
    record = '{ "applications": {}, "profiles": {} }';

  actions = JSON.parse(record);
  roles.import(actions);
  cb && cb();
};

module.exports = {
  reloadRoles: reloadRoles
};

