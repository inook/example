
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , mongoose = require('mongoose')
  , BearerStrategy = require(CONFIG.__libs + '/strategies').BearerStrategy;

/*!
 * Models.
 */

var Token = mongoose.model('Token');

/*!
 * Expose strategy.
 */

module.exports = new BearerStrategy({}, function (token, done) {
  process.nextTick(function () {
    Token.findOne({ token: token })
    .populate('_user')
    .exec(function (err, token) {
      if (!token) return done(null, false);
      done(err, token._user, { scope: 'all' });
    });
  });
});
