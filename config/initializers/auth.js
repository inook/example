
/*!
 * Module dependencies.
 */

var CONFIG = require('config')
  , fs = require('fs')
  , path = require('path')
  , mongoose = require('mongoose')
  , passport = require('passport')
  , roles = require('roles')
  , crypto = require('crypto');

/*!
 * Models.
 */

var User = require(CONFIG.__models + '/User');

/*!
 * RegExps.
 */

var jsFileRegexp = new RegExp('^[\\w]+\\.js$');

/**
 * Find user by id
 *
 * @param {Object} User
 * @param {Function} Callback
 */

function findById(id, cb) {
  User.findById(id, cb);
}

/*!
 * Serializers.
 */

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  findById(id, done);
});

/*!
 * Load strategies.
 */

fs.readdirSync(CONFIG.__strategies).forEach(function (strategyName) {
  if (jsFileRegexp && !jsFileRegexp.test(strategyName)) return;
  var strategy = require(path.join(CONFIG.__strategies, strategyName));
  passport.use(strategy);
});

/*!
 * Expose.
 */

module.exports = passport;
