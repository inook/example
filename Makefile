REPORTER = spec
BIN = ./node_modules/.bin

test:
	@$(BIN)/mocha \
		--reporter dot\
		--ui bdd

test-w:
	@$(BIN)/mocha \
		--reporter $(REPORTER)\
		--ui bdd\
		--watch

.PHONY: test
