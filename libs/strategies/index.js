
module.exports = process.env.STRATEGIES_COV
  ? require('./lib-cov/strategies')
  : require('./lib/strategies');

