
/**
 * Module dependencies.
 */

var BearerStrategy = require('./bearer');

/**
 * Define stretegies object.
 */

exports = module.exports = {};

// Custom bearer strategy.
exports.BearerStrategy = BearerStrategy;

