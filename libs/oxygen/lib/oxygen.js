
/**
 * Module dependencies.
 */

var obj = require('./object');

/**
 * Define oxygen.js object.
 */

exports = module.exports = {};

exports.Object = exports.object = obj;
