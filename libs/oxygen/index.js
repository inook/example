
module.exports = process.env.OXYGEN_COV
  ? require('./lib-cov/oxygen')
  : require('./lib/oxygen');
